# React Microfrontend Proof of Concept

The project is made out of several other projects that are running separately.

The following projects are essential/shared:

1. /styleguide: This is where our ui library will be implemented. It should be a generic ui library (mostly atoms and molecules).
2. /root-config: This is where the main app that handles the routing for the project's starting point. This is the one that the user actually faces. It depends on the `styleguide` and `shared-dependencies` as it uses the map inside shared-dependencies mapping file to understand the entry point for the other projects. This project also handles the url routing for each project.
3. /shared-dependencies: This is where the import map file that defines all the projects and where their urls do exist. It a simple json file that can be hosted on a bucket/cdn and it should be unique per each environment as it acts as the router for the projects not pages (ex: 1st level of sub route).

The following projects are optional/custom:

1. /api: This is where we handle our generic api functions. It'll be mostly helper functions to call the apis. (Have to be generic)
2. /navbar: An example micro frontend project.
3. /people: An example micro frontend project.
4. /planets: An example micro frontend project.

### Usage

0. Install Dependencies
   Run `yarn` inside all the folders

1. Run **shared-dependencies** project (default port on 5000. don't change this!)
   `cd shared-dependencies && yarn start`

1. Run **root-config** project (default port on 9000, to customize edit the start script in package.json)
   `cd root-config && yarn start`

1. Run **styleguide** project
   `cd styleguide && yarn start --port 9001`

1. Run **api** project
   `cd api && yarn start --port 9002`

1. Run **navbar** project
   `cd navbar && yarn start --port 9003`

1. Run **people** project
   `cd people && yarn start --port 9004`

1. Run **planets** project
   `cd planets && yarn start --port 9005`

### Notes

- We shouldn't use a global state management (redux) and even if we used any, it should limited to its project. As an alternative, we can use React's Context API (useReducer) for handling component's state.
- Microfrontend projects should not be dependent on other projects except the essential ones. However, it's possible too but this violates the principles of microfrontends/microservices. It should be very minimal though.
- Any microfrontend project can handle its routes inside it's root component as long as it starts with its own 1st level sub route. Otherwise, it'll not work as the root-config project will intercept it and will not resolve the microfrontend's project root component.
- For sharing ui state between projects, we should conside an event emitter to pass those data between the components as a best practice. (ex: Observables like RxJS or browser's native CustomEvents emitter system)
- For sharing components between projects, check the inter-app communication part in the docs: https://single-spa.js.org/docs/recommended-setup#inter-app-communication
- For more details about the concept, please visit single-spa docs through this link: https://single-spa.js.org/docs/getting-started-overview
